﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    class Platform
    {
        public GameObject go;
        public Vector3 start_position;
        public Vector3 end_position;
        public Quaternion start_rotation;
    }

    private GameObject[] syncObjects;

    Rigidbody rigidbody;
    bool is_grounded;

    protected Camera m_camera;

    GameObject clone_prefab;
    List<GameObject> clones = new List<GameObject>();

    Vector3 start_position;
    Quaternion start_rotation;

    Vector3 start_position_camera;
    Quaternion start_rotation_camera;

    bool recording = false;
    bool e_pressed = false;

    List<float> current_vertical_inputs = new List<float>();
    List<float> current_horizontal_inputs = new List<float>();
    List<float> current_jump_inputs = new List<float>();
    List<float> current_run_inputs = new List<float>();
    List<Vector3> current_cam_positions = new List<Vector3>();
    List<Quaternion> current_cam_rotations = new List<Quaternion>();

    public List<Vector3> current_position_inputs = new List<Vector3>();
    public List<Quaternion> current_rotation_inputs = new List<Quaternion>();

    List<Platform> platforms = new List<Platform>();

    int nb_times_platform_can_move = 5000 /*(int)Math.Pow(10.0, 4.0)*/;
    int nb_times_platform_have_moved = 0;

    Vector3 initial_position;

    float nb_frames_done = -Mathf.PI / 2f;

    // Start is called before the first frame update
    void Start()
    {
        m_camera = GetComponentInChildren<Camera>();
        rigidbody = GetComponent<Rigidbody>();

        clone_prefab = (GameObject)Resources.Load("clone", typeof(GameObject));
        List<GameObject> platforms_go = new List<GameObject>(GameObject.FindGameObjectsWithTag("MovingPlatforms"));
        foreach (GameObject go in platforms_go)
        {
            Platform pl = new Platform();
            pl.go = go;
            pl.start_position = go.transform.position;
            pl.end_position = go.transform.position + new Vector3(0f, 0f, 5f);
            pl.start_rotation = go.transform.rotation;
            platforms.Add(pl);
        }
        initial_position = transform.position;

        syncObjects = GameObject.FindGameObjectsWithTag("SyncObject");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            e_pressed = true;
            
        }

        if (Input.GetKeyDown("a"))
            transform.position = initial_position;

        if (Input.GetKeyDown("r"))
        {
            foreach (GameObject clone in clones)
            {
                CloneScriptClem cloneScript = clone.GetComponent<CloneScriptClem>();
                clone.transform.position = cloneScript.start_position;
                clone.transform.rotation = cloneScript.start_rotation;
                cloneScript.current_frame = 0;

                nb_frames_done = -Mathf.PI / 2f;

                foreach (GameObject g in syncObjects)
                    g.GetComponent<SyncObject>().Revert();
            }
            foreach (Platform platform in platforms)
            {
                platform.go.transform.position = platform.start_position;
                platform.go.transform.rotation = platform.start_rotation;
                nb_frames_done = -Mathf.PI / 2f;
            }
        }
    }

    private void LateUpdate()
    {
        /*if (recording)
        {
            current_cam_positions.Add(m_camera.transform.position);
            current_cam_rotations.Add(m_camera.transform.rotation);
        }*/
    }

    private void FixedUpdate()
    {
        Vector3 cam_forward_direction = transform.position - m_camera.transform.position;
        cam_forward_direction.y = 0f;
        cam_forward_direction.Normalize();
        Vector3 cam_right_direction = -Vector3.Cross(cam_forward_direction, Vector3.up);

        float speed = Input.GetAxis("Run") + 2f;

        transform.position += cam_forward_direction * Input.GetAxis("Vertical") * 0.1f * speed;
        transform.position += cam_right_direction * Input.GetAxis("Horizontal") * 0.1f * speed;

        Vector3 alignement = transform.forward;

        float input_vertical = Input.GetAxis("Vertical");
        if (input_vertical != 0f)
        {
            alignement = Mathf.Sign(input_vertical) * cam_forward_direction;
        }

        float input_horizontal = Input.GetAxis("Horizontal");
        if (input_horizontal != 0f)
        {
            alignement = Mathf.Sign(input_horizontal) * cam_right_direction;
        }

        if (input_horizontal != 0f && input_vertical != 0f)
            alignement = Mathf.Sign(input_vertical) * cam_forward_direction + Mathf.Sign(input_horizontal) * cam_right_direction;

        Quaternion rot = Quaternion.LookRotation(alignement);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.1f);

        float input_space = Input.GetAxis("Jump");
        if (is_grounded && input_space != 0f)
            rigidbody.AddForce(Vector3.up * 1000f);

        if (e_pressed)
        {
            if (!recording)
            {
                start_position = transform.position;
                start_rotation = transform.rotation;

                start_position_camera = m_camera.transform.position;
                start_rotation_camera = m_camera.transform.rotation;

                foreach (Platform platform in platforms)
                {
                    platform.go.transform.position = platform.start_position;
                    platform.go.transform.rotation = platform.start_rotation;
                    nb_frames_done = -Mathf.PI / 2f;
                }
                nb_times_platform_have_moved = 0;

                foreach (GameObject g in syncObjects)
                    g.GetComponent<SyncObject>().Revert();

            }
            else
            {
                {
                    GameObject new_clone = Instantiate(clone_prefab, start_position, start_rotation);
                    CloneScriptClem cloneScript = new_clone.GetComponent<CloneScriptClem>();

                    cloneScript.start_position = start_position;
                    cloneScript.start_rotation = start_rotation;
                    cloneScript.start_position_camera = start_position_camera;
                    cloneScript.start_rotation_camera = start_rotation_camera;

                    cloneScript.position_inputs = new List<Vector3>(current_position_inputs);
                    cloneScript.rotation_inputs = new List<Quaternion>(current_rotation_inputs);

                    cloneScript.horizontal_inputs = new List<float>(current_horizontal_inputs);
                    cloneScript.vertical_inputs = new List<float>(current_vertical_inputs);
                    cloneScript.jump_inputs = new List<float>(current_jump_inputs);
                    cloneScript.run_inputs = new List<float>(current_run_inputs);
                    cloneScript.cam_positions = new List<Vector3>(current_cam_positions);
                    cloneScript.cam_rotations = new List<Quaternion>(current_cam_rotations);

                    Physics.IgnoreCollision(new_clone.GetComponent<Collider>(), GetComponent<Collider>());

                    foreach (GameObject clone in clones)
                    {
                        Physics.IgnoreCollision(new_clone.GetComponent<Collider>(), clone.GetComponent<Collider>());
                    }

                    clones.Add(new_clone);
                }

                current_position_inputs.Clear();
                current_rotation_inputs.Clear();
                current_horizontal_inputs.Clear();
                current_vertical_inputs.Clear();
                current_jump_inputs.Clear();
                current_run_inputs.Clear();
                current_cam_positions.Clear();
                current_cam_rotations.Clear();

                foreach (GameObject clone in clones)
                {
                    CloneScriptClem cloneScript = clone.GetComponent<CloneScriptClem>();
                    clone.transform.position = cloneScript.start_position;
                    clone.transform.rotation = cloneScript.start_rotation;
                    cloneScript.current_frame = 0;
                }

                foreach (Platform platform in platforms)
                {
                    platform.go.transform.position = platform.start_position;
                    platform.go.transform.rotation = platform.start_rotation;
                    nb_frames_done = -Mathf.PI / 2f;
                }
                foreach (GameObject g in syncObjects)
                    g.GetComponent<SyncObject>().Revert();
                nb_times_platform_have_moved = 0;
            }
            recording = !recording;
        }
        
        if (recording)
        {
            current_vertical_inputs.Add(Input.GetAxis("Vertical"));
            current_horizontal_inputs.Add(Input.GetAxis("Horizontal"));
            current_jump_inputs.Add(Input.GetAxis("Jump"));
            current_run_inputs.Add(Input.GetAxis("Run"));

            current_cam_positions.Add(m_camera.transform.position);
            current_cam_rotations.Add(m_camera.transform.rotation);

            current_position_inputs.Add(transform.position);
            current_rotation_inputs.Add(transform.rotation);
        }

        if (e_pressed)
            e_pressed = !e_pressed;

        foreach (Platform platform in platforms)
        {
            float interpol = (Mathf.Sin(nb_frames_done) + 1f) / 2f;
            platform.go.transform.position = Vector3.Lerp(platform.start_position, platform.end_position, interpol);
        }
        nb_frames_done += 0.01f;
    }

    void OnCollisionEnter(Collision collision)
    {
        is_grounded = true;
        if (collision.gameObject.tag == "MovingPlatforms" || collision.gameObject.tag == "SyncObject")
        {
            transform.parent = collision.gameObject.transform;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "MovingPlatforms" || collision.gameObject.tag == "SyncObject")
        {
            transform.parent = null;
        }
        is_grounded = false;
    }

    void OnCollisionStay(Collision collision)
    {
        is_grounded = true;
    }
}