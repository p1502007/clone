﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLight : TriggerObject
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void triggerEnter()
    {
        GetComponent<Light>().color = Color.green;
    }

    public override void triggerExit()
    {
        GetComponent<Light>().color = Color.red;
    }

    public override void triggerStay()
    {
        GetComponent<Light>().color = Color.green;
    }
}
