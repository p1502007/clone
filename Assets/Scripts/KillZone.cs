﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    public Vector3 ResetPosition;

    public void OnTriggerEnter(Collider other)
    {
        other.transform.position = ResetPosition;
    }
}
