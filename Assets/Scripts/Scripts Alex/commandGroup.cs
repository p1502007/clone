﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandGroup
{
    List<Command> liste;


    public CommandGroup(){
        liste = new List<Command>();
    }
    public CommandGroup(List<Command> l){
        liste = l;
    }
    public void Add(Command c){
        liste.Add(c);
    }

    public bool Equals(CommandGroup obj)
    {
        if (liste.Count!= obj.liste.Count){
            return false;
        }
        foreach(Command c in liste){
            if (!(obj.liste.Contains(c))){
                return false;
            }
        }
        return true;
    }

    public void Execute(controller cc){
        foreach(Command c in liste){
            c.Execute(cc);
        }
    }
}
