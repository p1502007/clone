﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Command
{
    public abstract void Execute(controller c);
}


public class FrowardCommand : Command
{

    public override void Execute(controller c){
        c.Forward();
    }
}

public class BackCommand : Command
{
    public override void Execute(controller c){
        c.Back();
    }
}

public class LeftCommand : Command
{
    public override void Execute(controller c){
        c.Left();
    }
}

public class RightCommand : Command
{
    public override void Execute(controller c){
        c.Right();
    }
}

public class RecordLightCommand : Command
{
    public override void Execute(controller c) {
        c.RecordLight();
    }
}

public class RecordLightPlatform : Command
{
    public override void Execute(controller c) {
        c.RecordPlatform();
    }
}

public class JumpCommand : Command
{
    public override void Execute(controller c) {
        c.Jump();
    }
}

public class RevertCommand : Command
{
    public override void Execute(controller c){
        c.Revert();
    }
}

public class ClearCloneCommand : Command
{
    public override void Execute(controller c){
        c.ClearAll();
    }
}

public class CleaOneCloneCommand : Command
{
    public override void Execute(controller c){
        c.ClearLast();
    }
}