﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CloneScript : MonoBehaviour
{
    // Start is called before the first frame update
    public PointInTime[] memoire;
    public DirectionInTime[] directions;
    private float elapsedTime = 0f;
    
    private float elapsedTimeDirection = 0f;
    public int nb_elem;
    public int test;
    
    public Vector3 startPos;
    public Quaternion startRot;
    private int currentKeyframe = 0;
    private float sizeKeyframe;
    bool running = false;
    protected Animator m_animator;

    protected Color startColor;
    public Color endColor;

    protected Renderer cubeRenderer;
    protected Light m_light;
    void Awake(){
        cubeRenderer = GetComponentInChildren<Renderer>(); // c'est pas bo
        m_light = GetComponentInChildren<Light>(); // c'est pas bo
        startColor = cubeRenderer.material.GetColor("_EmissionColor");
        Debug.Log("start color : " + startColor);

        //endColor = Color.red;
        m_animator = GetComponentInChildren<Animator>();
        m_animator.speed = 0f;
        CloneScript[] clones = FindObjectsOfType<CloneScript>();
        foreach(CloneScript c in clones){
            Physics.IgnoreCollision(c.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }

    public void setMemoire(List<PointInTime> _memoire,float _sizeKeyframe){
        currentKeyframe = 0;
        sizeKeyframe = _sizeKeyframe;
        memoire = new PointInTime[_memoire.Count];
        _memoire.CopyTo(memoire);
        elapsedTime = 0f;
        transform.position = startPos;
        transform.rotation = startRot;
    }

    public void Go(){
        transform.parent=GameObject.FindGameObjectWithTag("Level").transform;
        currentKeyframe = 0;
        elapsedTime = 0f;
        transform.position = startPos;
        transform.rotation = startRot;
        running =true;
    }

    public void Revert(){
        transform.parent=GameObject.FindGameObjectWithTag("Level").transform;
        currentKeyframe = 0;
        elapsedTime = 0f;
        transform.position = startPos;
        transform.rotation = startRot;
        running =false;
        Rewind();
    }
    
    // Update is called once per frame
    void Update()
    {
        //Debug.Log("running : " + running);
        if (running){
            float m =((float) memoire.Length-1) * (float)sizeKeyframe * Time.fixedDeltaTime;
            if (elapsedTime<=m){    

                currentKeyframe = Mathf.FloorToInt(elapsedTime / ((float) sizeKeyframe * Time.fixedDeltaTime ));
                float reste = elapsedTime % (sizeKeyframe * Time.fixedDeltaTime);
                float t = reste/((float) sizeKeyframe * Time.fixedDeltaTime );
                PointInTime p = PointInTime.Lerp(memoire[currentKeyframe],memoire[currentKeyframe+1],t);
                //transform.Translate(p.position-transform.position);
                float nzTime = Mathf.Lerp(memoire[currentKeyframe].info.normalizedTime,memoire[currentKeyframe+1].info.normalizedTime,t);
                transform.position = p.position;
                transform.rotation = p.rotation;
                transform.GetChild(0).rotation = p.charRotation;
                m_animator.Play(memoire[currentKeyframe].info.fullPathHash,0,nzTime);
            }
            else{
                endRecording();
                running = false;
            }
            elapsedTime+=Time.deltaTime;
        }
    }

    protected virtual void endRecording(){
        m_animator.speed = 1f;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints ^ RigidbodyConstraints.FreezePosition;
        cubeRenderer.material.SetColor("_EmissionColor",endColor);
        m_light.color = endColor;
    }

    protected virtual void Rewind(){
        m_animator.speed= 0f;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints & ~RigidbodyConstraints.FreezePosition;
        cubeRenderer.material.SetColor("_EmissionColor",startColor);
        m_light.color = startColor;
    }

}
