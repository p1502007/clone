﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Synchroniser : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject[] syncObjects;
    void Start()
    {    
        syncObjects = GameObject.FindGameObjectsWithTag("SyncObject");
        foreach(GameObject g in syncObjects){
            Debug.Log("initializing Synchroniser ...");
            g.GetComponent<SyncObject>().Init();
            Debug.Log("Synchroniser initialized");
        }
    }



    public void GetAll(){
        syncObjects = GameObject.FindGameObjectsWithTag("SyncObject");
    }

    // Update is called once per frame
    public void ResetAll()
    {
        GetAll();
        foreach (GameObject g in syncObjects){
            Debug.Log("Reverting ...");
            g.GetComponent<SyncObject>().Revert();
            Debug.Log("Reverted");
        }
    }

    public void StartAll()
    {
        foreach (GameObject g in syncObjects){
            Debug.Log("Starting all ...");
            //g.GetComponent<SyncObject>().Start()
        }
    }
}
