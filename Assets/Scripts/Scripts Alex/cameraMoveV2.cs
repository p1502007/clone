﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMoveV2 : MonoBehaviour
{
    public Transform target;
    public float rotationSpeed = 50f;
    public float distance = 5f;
    public float height = 5f;
    public float distanceMin = 1f;
    public float distanceMax = 10f;
    Vector3 m_camera_target;
    float yOrbit = 0f;
    private int layermask;
    public void Start(){
        layermask = Physics.DefaultRaycastLayers - LayerMask.NameToLayer("SyncObject");
    }

    public void Update()
    {
        m_camera_target = target.position + Vector3.up * 1.2f;
        float dx = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
        float dy = Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime;
        Vector3 resultingPos = transform.TransformPoint(-dx,-dy,0);
        //if (Vector3.Angle(resultingPos,Vector3.up)>=20 && Vector3.Angle(resultingPos,Vector3.up)<=90)
        CheckBoundaries(resultingPos, ref dy);
        transform.Translate(-dx,-dy,0,Space.Self);
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
        Vector3 player2cam  = (transform.position - m_camera_target ).normalized; 
        transform.position =m_camera_target + player2cam * distance;
        RaycastHit hit;
        if (Physics.Linecast(m_camera_target, transform.position, out hit,layermask))
        {
            //distance = hit.distance;
            transform.position =m_camera_target + player2cam * (hit.distance - 0.3f);
        }
        transform.LookAt(m_camera_target, Vector3.up);
    }

    void CheckBoundaries(Vector3 resultPos ,ref float  dy){
        Vector3 player2cam  = (resultPos - m_camera_target ).normalized; 
        float currentAngle = Vector3.Angle(Vector3.up, player2cam);
        if (currentAngle<30 && dy<=0)
            dy=0;
        if (currentAngle>100 && dy>0)
            dy=0;
    }
}
