﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charController : controller
{
    // Start is called before the first frame update
    private RecordScript recordScript;

    public List<Command> commandBuffer;

    

    private Rigidbody rb;

    private Synchroniser synchroniser;

    private bool isJumping;

    Animator m_animator;
    public GameObject cloneLight;
    public GameObject clonePlatform;
    public void Start(){
        base.Init();
        synchroniser = GameObject.FindGameObjectWithTag("Synchroniser").GetComponent<Synchroniser>();
        recordScript = GetComponent<RecordScript>();
        m_animator = GetComponentInChildren<Animator>();
    }
    public override void Forward()
    {
        ps.forward = 1.0f;
    }

    public override void Left()
    {
        ps.left = 1.0f ;
    }

    public override void Right()
    {
        ps.right = 1.0f;
    }

    public override void Back(){
        ps.back = 1.0f;
    }

    public override void RecordLight(){
        if (!recordScript.recording){
            m_animator.SetTrigger("record");
            if (recordScript.handleRecord(cloneLight))
            Revert();
        }
            
        else{
            StartCoroutine(pasToucheLight(gameObject));
        }   
    }


    public override void RecordPlatform(){
        if (!recordScript.recording){
            m_animator.SetTrigger("record");
            if (recordScript.handleRecord(clonePlatform))
            Revert();
        }
            
        else{
            StartCoroutine(pasTouchePlatform(gameObject));
        }   
    }

    public override void Jump(){
        if (ps.isGrounded){
            ps.jump = 1.0f;
            ps.isGrounded=false;
        }
    }
    
    public override void Revert(){
        synchroniser.ResetAll();
    }

    public override void ClearAll(){
        recordScript.removeAllClones();
    }

    public override void ClearLast(){
        recordScript.removeLastClone();
    }

    IEnumerator pasToucheLight(GameObject player){
        player.GetComponent<playerScriptV2>().enabled = false;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints ^ RigidbodyConstraints.FreezePosition;
        m_animator.SetTrigger("endRecord");
        recordScript.handleRecord(cloneLight);
        yield return new WaitUntil(() => m_animator.GetCurrentAnimatorStateInfo(0).IsName("EndRecord") && m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime>0.8f);
        //recordScript.handleRecord();
        player.GetComponent<playerScriptV2>().enabled = true;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints & ~RigidbodyConstraints.FreezePosition;
        GetComponent<RecordScript>().Revert();
        Revert();
    }


    IEnumerator pasTouchePlatform(GameObject player){
        player.GetComponent<playerScriptV2>().enabled = false;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints ^ RigidbodyConstraints.FreezePosition;
        m_animator.SetTrigger("endRecord");
        recordScript.handleRecord(clonePlatform);
        yield return new WaitUntil(() => m_animator.GetCurrentAnimatorStateInfo(0).IsName("EndRecord") && m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime>0.8f);
        //recordScript.handleRecord();
        player.GetComponent<playerScriptV2>().enabled = true;
        GetComponent<Rigidbody>().constraints = GetComponent<Rigidbody>().constraints & ~RigidbodyConstraints.FreezePosition;
        GetComponent<RecordScript>().Revert();
        Revert();
    }
}

