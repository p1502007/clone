﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

public class Button : MonoBehaviour
{
    public GameObject[] objectsToActivate ;

    protected int nbrObjOnTrigger = 0;
    protected float offset = 0.01f;


    protected virtual void OnTriggerEnter(Collider other)
    {

        nbrObjOnTrigger++;

        // Envoie le trigger enter seulement quand il y a 1 objet sur le bouton
        if (nbrObjOnTrigger == 1)
        {
            //Vector3 deplacement = new Vector3(0f, -GetComponent<MeshFilter>().mesh.bounds.max.y + offset, 0f);
            //transform.Translate(deplacement);

            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].GetComponent<TriggerObject>().triggerEnter();
            }
        }
           
    }

    protected virtual void OnTriggerExit(Collider other)
    {

        nbrObjOnTrigger--;

        // Envoie le trigger exit seulement seulement quand il n'y a plus rien sur le bouton
        if(nbrObjOnTrigger == 0)
        {
            //Vector3 deplacement = new Vector3(0f, GetComponent<MeshFilter>().mesh.bounds.max.y - offset, 0f);
            //transform.Translate(deplacement);
            
            

            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].GetComponent<TriggerObject>().triggerExit();
            }
        }

    }

    protected virtual void OnTriggerStay(Collider other)
    {
        for (int i = 0; i < objectsToActivate.Length; i++)
        {
            objectsToActivate[i].GetComponent<TriggerObject>().triggerStay();
        }
            
    }

}
