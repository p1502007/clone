﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneScriptClem : MonoBehaviour
{
    Rigidbody rigidbody;
    public Vector3 start_position;
    public Quaternion start_rotation;
    public Vector3 start_position_camera;
    public Quaternion start_rotation_camera;

    public List<Vector3> position_inputs = new List<Vector3>();
    public List<Quaternion> rotation_inputs = new List<Quaternion>();

    public List<Vector3> cam_positions = new List<Vector3>();
    public List<Quaternion> cam_rotations = new List<Quaternion>();
    public List<float> vertical_inputs = new List<float>();
    public List<float> horizontal_inputs = new List<float>();
    public List<float> jump_inputs = new List<float>();
    public List<float> run_inputs = new List<float>();

    public int current_frame = 0;

    bool is_grounded;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        /*Debug.Log("cam_positions size : " + cam_positions.Count);
        Debug.Log("cam_rotations size : " + cam_rotations.Count);
        Debug.Log("vertical_inputs size : " + vertical_inputs.Count);
        Debug.Log("horizontal_inputs size : " + horizontal_inputs.Count);
        Debug.Log("jump_inputs size : " + jump_inputs.Count);
        Debug.Log("run_inputs size : " + run_inputs.Count);*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (current_frame < vertical_inputs.Count)
        {
            //transform.position = position_inputs[current_frame];
            //transform.rotation = rotation_inputs[current_frame];

            Vector3 cam_forward_direction = transform.position - cam_positions[current_frame];
            cam_forward_direction.y = 0f;
            cam_forward_direction.Normalize();
            Vector3 cam_right_direction = -Vector3.Cross(cam_forward_direction, Vector3.up);

            float speed = run_inputs[current_frame] + 2f;

            transform.position += cam_forward_direction * vertical_inputs[current_frame] * 0.1f * speed;
            transform.position += cam_right_direction * horizontal_inputs[current_frame] * 0.1f * speed;

            Vector3 alignement = transform.forward;

            float input_vertical = vertical_inputs[current_frame];
            if (input_vertical != 0f)
            {
                alignement = Mathf.Sign(input_vertical) * cam_forward_direction;
            }

            float input_horizontal = horizontal_inputs[current_frame];
            if (input_horizontal != 0f)
            {
                alignement = Mathf.Sign(input_horizontal) * cam_right_direction;
            }

            if (input_horizontal != 0f && input_vertical != 0f)
                alignement = Mathf.Sign(input_vertical) * cam_forward_direction + Mathf.Sign(input_horizontal) * cam_right_direction;

            Quaternion rot = Quaternion.LookRotation(alignement);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.1f);

            float input_space = jump_inputs[current_frame];
            if (is_grounded && input_space != 0f)
                rigidbody.AddForce(Vector3.up * 1000f);


            current_frame++;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        is_grounded = true;
        if (collision.gameObject.tag == "MovingPlatforms")
        {
            transform.parent = collision.gameObject.transform;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "MovingPlatforms")
        {
            transform.parent = null;
        }
        is_grounded = false;
    }

    void OnCollisionStay(Collision collision)
    {
        is_grounded = true;
    }
}
