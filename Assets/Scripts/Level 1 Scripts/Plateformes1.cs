﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plateformes1 : PlateForme
{

    public override void triggerEnter()
    {

        float t = cumulDeltaTime / time;
        time *= 2;
        cumulDeltaTime = t * time;

    }

    public override void triggerExit()
    {

        float t = cumulDeltaTime / time;
        time /= 2;
        cumulDeltaTime = t * time;

    }

}
