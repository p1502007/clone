﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void triggerEnter()
    {

    }

    public virtual void triggerExit()
    {

    }

    public virtual void triggerStay()
    {

    }
}
