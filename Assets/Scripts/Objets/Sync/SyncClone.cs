﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncClone : SyncObject
{

    public override void Init(){

    } 
    public override void Revert(){
        GetComponent<CloneScript>().Revert();
        GetComponent<CloneScript>().Go();
    }
}
