﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateForme : TriggerObject
{

    public Vector3 startPosition;
    public Vector3 endPosition;
    public float time = 2f;
    [Range(0, 1)]
    public float phase = 0f;
    public int nbrTriggersToActivate = 1;
    

    protected float cumulDeltaTime = 0f;
    protected int compteurTriggersActifs = 0;


    // Start is called before the first frame update
    public virtual void Start()
    {
        cumulDeltaTime = phase;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if(nbrTriggersToActivate == 0)
        {
            if(transform.localPosition == startPosition || transform.localPosition == endPosition)
            {
                swapPos();
            }
        }

        goTo(endPosition, startPosition, Time.deltaTime);
    }

    public override void triggerEnter()
    {
        compteurTriggersActifs++;

        if (compteurTriggersActifs == nbrTriggersToActivate)
        {
            swapPos();
        } 
    }

    public override void triggerExit()
    {
        compteurTriggersActifs--;

        if(compteurTriggersActifs == nbrTriggersToActivate - 1)
        {
            swapPos();
        }
    }

    public virtual void swapPos()
    {
        Vector3 temp = startPosition;
        startPosition = endPosition;
        endPosition = temp;

        float t = cumulDeltaTime / time;
        cumulDeltaTime = (1 - t) * time;
    }

    public virtual void goTo(Vector3 start, Vector3 end, float deltaTime)
    {
        cumulDeltaTime += deltaTime;
        cumulDeltaTime = Mathf.Clamp(cumulDeltaTime, 0, time);

        transform.localPosition = Vector3.Lerp(start, end, cumulDeltaTime / time);
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEnter");
        other.transform.parent = transform;
    }

    public virtual void OnTriggerExit(Collider other)
    {
        other.transform.parent = GameObject.FindGameObjectsWithTag("Level")[0].transform;
    }

    public void setCumulDeltaTime(float n)
    {
        cumulDeltaTime = n;
    }
}
