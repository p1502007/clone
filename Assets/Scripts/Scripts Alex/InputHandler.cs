﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    // Start is called before the first frame update
    // Update is called once per frame
    Command buttonZ;
    Command buttonS;
    Command buttonQ;
    Command buttonD;

    Command buttonR;

    Command buttonSpace;

    Command buttonLShift;
    Command buttonC;
    Command buttonA;
    Command buttonE;
    Command buttonX;

    public playerScriptV2 player;

    void Start()
    {
        buttonZ = new FrowardCommand();
        buttonS = new BackCommand();
        buttonQ = new LeftCommand();
        buttonD = new RightCommand();
        buttonSpace = new JumpCommand();
        buttonLShift = new RecordLightCommand();
        buttonR = new RevertCommand();
        buttonC = new ClearCloneCommand();
        buttonA = new RecordLightCommand();
        buttonE = new RecordLightPlatform();
        buttonX = new CleaOneCloneCommand();
    }
    void Update()
    {
        CommandGroup c = new CommandGroup();
        if (Input.GetKey(KeyCode.Z)) c.Add(buttonZ);
        if (Input.GetKey(KeyCode.S)) c.Add(buttonS);
        if (Input.GetKey(KeyCode.Q)) c.Add(buttonQ);
        if (Input.GetKey(KeyCode.D)) c.Add(buttonD);
        if (Input.GetKeyDown(KeyCode.R)) c.Add(buttonR);
        if (Input.GetKeyDown(KeyCode.Space)) c.Add(buttonSpace);
        if (Input.GetKeyDown(KeyCode.LeftShift)) c.Add(buttonLShift);
        if (Input.GetKeyDown(KeyCode.C)) c.Add(buttonC);
        if (Input.GetKeyDown(KeyCode.A)) c.Add(buttonA);
        if (Input.GetKeyDown(KeyCode.E)) c.Add(buttonE);
        if (Input.GetKeyDown(KeyCode.X)) c.Add(buttonX);
        player.commandGroup = c;
    }
}
