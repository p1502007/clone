﻿using UnityEngine;

public class PointInTime
{
    public Quaternion rotation;
    public Vector3 position;
    public Quaternion charRotation;

    public AnimatorStateInfo info;

    public PointInTime(Vector3 _position, Quaternion _rotation,Quaternion forward,AnimatorStateInfo animInfo){
        rotation = _rotation;
        position = _position;
        charRotation = forward;
        info = animInfo;
    }
    public PointInTime(Transform t, Quaternion forward,AnimatorStateInfo animInfo){
        rotation = t.rotation;
        position = t.position;
        charRotation = forward;
        info = animInfo;
    }

    public static PointInTime Lerp(PointInTime p1, PointInTime p2, float t){
        PointInTime p =new PointInTime(
            Vector3.Lerp(p1.position,p2.position,t),
            Quaternion.Lerp(p1.rotation,p2.rotation,t),
            Quaternion.Lerp(p1.charRotation,p2.charRotation,t),
            p1.info);
        return p;
    }
}
