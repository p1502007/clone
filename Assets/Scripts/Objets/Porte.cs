﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porte : TriggerObject
{


    public bool open = true;
    public int nbrTriggersToActivate = 1;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float openingTime = 2f;
    public float closingTime = 2f;

    public AudioClip openingSound;
    public AudioClip closingSound;

    private bool ouverture = false;
    private int compteurTriggersActifs = 0;
    private float cumulDeltaTime = 0f;
    private AudioSource audioSource;
    private bool audioCanPlay = true;


    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.localPosition;
        cumulDeltaTime = openingTime;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        goTo(endPosition, startPosition, Time.deltaTime);
    }

    public override void triggerEnter()
    {
        compteurTriggersActifs++;

        if (compteurTriggersActifs == nbrTriggersToActivate)
        {
            swapPos();
            ouverture = true;

            audioSource.PlayOneShot(openingSound, 0.7f);
        }

    }

    public override void triggerExit()
    {
        compteurTriggersActifs--;

        if (compteurTriggersActifs == nbrTriggersToActivate - 1)
        {
            swapPos();
            ouverture = false;

            audioSource.PlayOneShot(closingSound, 0.7f);
        }

    }

    private void swapPos()
    {
        Vector3 temp = startPosition;
        startPosition = endPosition;
        endPosition = temp;

        if (ouverture)
        {
            float t = cumulDeltaTime / openingTime;
            cumulDeltaTime = (1f - t) * closingTime;
        }

        else
        {
            float t = cumulDeltaTime / closingTime;
            cumulDeltaTime = (1f - t) * openingTime;
        }

    }

    private void goTo(Vector3 start, Vector3 end, float deltaTime)
    {
        float time;

        if (ouverture)
        {
            time = openingTime;
        }
        else
        {
            time = closingTime;
        }

        cumulDeltaTime += deltaTime;
        cumulDeltaTime = Mathf.Clamp(cumulDeltaTime, 0, time);


        transform.localPosition = Vector3.Lerp(start, end, cumulDeltaTime / time);
    }
}
