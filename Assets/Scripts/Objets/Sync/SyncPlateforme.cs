﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncPlateforme : SyncObject
{
    float initPosition;
    Vector3 startPosition, endPosition;


    public override void Init()
    {
        initPosition = GetComponent<PlateForme>().phase;

        startPosition = GetComponent<PlateForme>().startPosition;
        endPosition = GetComponent<PlateForme>().endPosition;
    }

    public override void Revert()
    {
        GetComponent<PlateForme>().setCumulDeltaTime(initPosition);

        GetComponent<PlateForme>().startPosition = startPosition;
        GetComponent<PlateForme>().endPosition = endPosition;
    }
}
