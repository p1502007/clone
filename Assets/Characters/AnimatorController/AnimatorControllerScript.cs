﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorControllerScript : MonoBehaviour
{
    protected Animator m_animator;

    protected Camera m_camera;
    protected Vector3 m_camera_target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    float x = 0.0f;
    float y = 0.0f;

    float horizontalAdjust = 0.5f;
    float horizontalIncrement = 0.1f;

    float EPSILON = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        m_animator = GetComponent<Animator>();
        //m_animator.speed = 1.5f;
        m_animator.SetFloat("verticalSpeed", 0f);
        m_animator.SetFloat("horizontalSpeed", 0.5f);
        
        m_camera = GetComponentInChildren<Camera>();

        Vector3 angles = m_camera.transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    // Update is called once per frame
    void Update()
    {
        /*m_animator.SetFloat("verticalSpeed", Input.GetAxis("Vertical"));
        m_animator.SetFloat("horizontalSpeed", (Input.GetAxis("Horizontal") + 1f) / 2f);*/

        m_animator.SetFloat("run", Input.GetAxis("Run"));

        if (Input.GetKeyDown("space"))
            m_animator.SetTrigger("jump");

        if (System.Math.Abs(Input.GetAxis("Vertical")) > EPSILON)
        {
            m_animator.SetFloat("verticalSpeed", Math.Abs(Input.GetAxis("Vertical")));
            Vector3 cam_direction = m_camera.transform.forward;
            float speed = (Input.GetAxis("Vertical") + 1f) / 2f;
            // Si on appuie sur gauche
            if (speed < 0.5f)
                cam_direction = -cam_direction;

            Vector3 char_direction = transform.forward;
            // On projette sur le plan XZ
            cam_direction.y = 0;
            char_direction.y = 0;
            float angle = Vector3.SignedAngle(char_direction, cam_direction, Vector3.up);
            if (angle < -5f)
                horizontalAdjust = Mathf.Max(0.0f, horizontalAdjust - horizontalIncrement);
            else if (angle > 5f)
                horizontalAdjust = Mathf.Min(1f, horizontalAdjust + horizontalIncrement);
            else {
                if (horizontalAdjust > 0.5f)
                    horizontalAdjust = Mathf.Max(0.5f, horizontalAdjust - horizontalIncrement);
                else if (horizontalAdjust < 0.5f)
                    horizontalAdjust = Mathf.Min(0.5f, horizontalAdjust + horizontalIncrement);
            }
            //if (System.Math.Abs(Input.GetAxis("Horizontal")) < EPSILON)
                m_animator.SetFloat("horizontalSpeed", horizontalAdjust);
        }

        if (System.Math.Abs(Input.GetAxis("Horizontal")) > EPSILON)
        {
            m_animator.SetFloat("verticalSpeed", Math.Abs(Input.GetAxis("Horizontal")));
            Vector3 cam_direction = m_camera.transform.right;
            float speed = (Input.GetAxis("Horizontal") + 1f) / 2f;
            // Si on appuie sur gauche
            if (speed < 0.5f)
                cam_direction = - cam_direction;

            Vector3 char_direction = transform.forward;
            // On projette sur le plan XZ
            cam_direction.y = 0;
            char_direction.y = 0;
            float angle = Vector3.SignedAngle(char_direction, cam_direction, Vector3.up);
            if (angle < -5f)
                horizontalAdjust = Mathf.Max(0.0f, horizontalAdjust - horizontalIncrement);
            else if (angle > 5f)
                horizontalAdjust = Mathf.Min(1f, horizontalAdjust + horizontalIncrement);
            else
            {
                if (horizontalAdjust > 0.5f)
                    horizontalAdjust = Mathf.Max(0.5f, horizontalAdjust - horizontalIncrement);
                else if (horizontalAdjust < 0.5f)
                    horizontalAdjust = Mathf.Min(0.5f, horizontalAdjust + horizontalIncrement);
            }
            //if (System.Math.Abs(Input.GetAxis("Horizontal")) < EPSILON)
            m_animator.SetFloat("horizontalSpeed", horizontalAdjust);
        }

        /*if (Math.Abs(m_animator.GetFloat("horizontalSpeed") - 0.5f) > EPSILON)
            m_animator.speed = 3f;
        else
            m_animator.speed = 1f;
            */
        //Debug.Log("horizontalAdjust = " + horizontalAdjust);        
    }

    private void LateUpdate()
    {
        m_camera_target = transform.position + Vector3.up * 2f;

        x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

        y = ClampAngle(y, yMinLimit, yMaxLimit);

        Quaternion rotation = Quaternion.Euler(y, x, 0);

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

        RaycastHit hit;
        if (Physics.Linecast(m_camera_target, m_camera.transform.position, out hit))
        {
            distance -= hit.distance;
        }
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + m_camera_target;

        m_camera.transform.rotation = rotation;
        m_camera.transform.position = position;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
