﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller : MonoBehaviour
{
    // Start is called before the first frame update
    protected playerScriptV2 ps;
    public void Init(){
        ps = GetComponent<playerScriptV2>();
    }
    public virtual void Forward()
    {
    }

    public virtual void Left()
    {
    }

    public virtual void Right()
    {

    }

    public virtual void Back(){
    }

    public virtual void RecordLight(){
    }
    public virtual void RecordPlatform(){
        
    }

    public virtual void Jump(){
        
    }

    public virtual void Revert(){
        
    }
    public virtual void ClearAll(){

    }

    public virtual void ClearLast(){
        
    }
}
