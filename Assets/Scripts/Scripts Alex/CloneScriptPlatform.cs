﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneScriptPlatform : CloneScript
{
    protected override void endRecording(){
        base.endRecording();
        Physics.IgnoreCollision(GetComponent<Collider>(), GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>(),false);
    }

    protected override void Rewind(){
        base.Rewind();
        Physics.IgnoreCollision(GetComponent<Collider>(), GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>(),true);
    }
}
