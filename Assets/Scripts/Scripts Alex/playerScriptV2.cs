﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScriptV2 : MonoBehaviour
{
            // Start is called before the first frame update
    [HideInInspector]
    public float forward=0f;
    [HideInInspector]
    public float right= 0f;
    [HideInInspector]
    public float left = 0f;
    [HideInInspector]
    public float back= 0f;
    [HideInInspector]
    public float jump = 0f;
    public float jumpForce = 10f;
    public float rotSpeed = 0.2f;
    public float moveSpeed = 20f;

    [Range(1f,5f)]
    public float fallMultiplier = 1f;

    [Range(1f,5f)]
    public float lowJumpMultiplier = 1f;
    public GameObject cam;
    public GameObject character;

    [HideInInspector]
    public Vector3 forwardDirection;
    Rigidbody rb;

    [HideInInspector]
    public CommandGroup commandGroup;
    [HideInInspector]
    public bool isGrounded=true;
    void Start()
    {
        commandGroup = new CommandGroup();
        rb = GetComponent<Rigidbody>();
        forwardDirection = Vector3.forward;        
    }

    // Update is called once per frame
    void Update()
    {
        if (cam != null){
            forwardDirection = (transform.position - cam.transform.position);
        }
        
        forwardDirection.y = 0f;
        forwardDirection.Normalize();


        commandGroup.Execute(GetComponent<controller>());

        
        Vector3 forwardMovement = forwardDirection*(forward-back);
        Vector3 lateralMovement = Vector3.Cross(forwardDirection,Vector3.up) * (left-right);

        Vector3 move = (forwardMovement + lateralMovement).normalized;
        
        if (rb.velocity.y<0){
            rb.velocity += Vector3.up*  Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        if (rb.velocity.y<3){
            rb.velocity += Vector3.up * jump * jumpForce ;
        }
        if (rb.velocity.y>0){
            rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier -1) * Time.deltaTime;
        }
        transform.Translate(move * moveSpeed * Time.deltaTime,Space.World) ;
        Vector3 rot = Vector3.RotateTowards(character.transform.forward,move, rotSpeed,0);
        character.transform.rotation = Quaternion.LookRotation(rot);
        
        forward = 0f;
        back = 0f;
        left = 0f;
        right = 0f;
        jump = 0f;
    }

    void OnCollisionStay(Collision collider)
    {
        //Debug.Log("collision stay");
        //isGrounded = true;
        CheckIfGrounded();
    }

    void OnCollisionExit(Collision collider)
    {
        //Debug.Log("exit");

        isGrounded = false;
    }
    private void CheckIfGrounded()
    {
        RaycastHit[] hits;

        //We raycast down 1 pixel from this position to check for a collider
        Vector3 positionToCheck = transform.position;
        
        hits = Physics.RaycastAll(positionToCheck, new Vector3 (0, -1,0), 1.5f);
        //if a collider was hit, we are grounded
        if (hits.Length > 0) {
            isGrounded = true;
        }
        else{
            
            isGrounded = false;
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position,transform.position+new Vector3(0,-1,0));   
    }

    public void SetPosRot(PointInTime p){
        transform.position = p.position;
        transform.rotation = p.rotation;
    }

}
