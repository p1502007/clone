﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloneController : controller
{
    // Start is called before the first frame update
    public void Start(){
        base.Init();

    }
    public override void Forward()
    {
        ps.forward = 1.0f;
    }

    public override void Left()
    {
        ps.left = 1.0f ;
    }

    public override void Right()
    {
        ps.right = 1.0f;
    }

    public override void Back(){
        ps.back = 1.0f;
    }


    public override void Jump(){
        if (ps.isGrounded){
            ps.jump = 1.0f;
            ps.isGrounded=false;
        }
    }
}
