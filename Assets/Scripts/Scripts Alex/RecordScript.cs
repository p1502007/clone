﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordScript : MonoBehaviour
{
    

    public bool allowedToRecord = false;
    public bool recording=false;
    public float maxTime = 5f;
    float cumulDeltaTime = 0f;
    private int sizeKeyframe;

    public AudioClip startRecordSound;
    public AudioClip stopRecordSound;
    private AudioSource audioSource;

    private int iter;
    
    private GameObject clone;
    private List<PointInTime> memoire;

    private playerScriptV2 ps;
    Vector3 startPosition;
    Quaternion startRotation;
    private int nb_elem = 0;

    public ZoneClonage zone;
    Animator m_animator;
    Renderer m_renderer;
    float t = 0.5f;
    float sens = -1f;
    float flickSpeed = 0.5f;
    private Color minIntensity;
    private Color maxIntensity;

    private List<GameObject> clones;
    void Start()
    {
        clones = new List<GameObject>();
        m_animator = GetComponentInChildren<Animator>();
        m_renderer = GetComponentInChildren<Renderer>();
        maxIntensity = m_renderer.material.GetColor("_EmissionColor");
        minIntensity = maxIntensity* 0.5f;
        audioSource = GetComponent<AudioSource>();
        
        sizeKeyframe = 5; // toutes les 5 executions de fixedUpdate
        ps = GetComponent<playerScriptV2>();
        memoire = new List<PointInTime>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (recording){
            cumulDeltaTime += Time.fixedDeltaTime;
            if(iter>sizeKeyframe){
                memoire.Add(new PointInTime(transform,ps.character.transform.rotation,m_animator.GetCurrentAnimatorStateInfo(0)));
                iter=0;
            }   
            iter++;
        }
    }

    void Update(){
       
        if (recording){
            t+=Time.deltaTime * sens;
            if (t>flickSpeed|| t<0f){
                sens *=-1f;
            }   
        }
        else{
            t=flickSpeed;
        }
        m_renderer.material.SetColor("_EmissionColor",Color.Lerp(minIntensity,maxIntensity,Mathf.Lerp(0,1/flickSpeed,t)));
    }

    public bool handleRecord(GameObject _clone){
        if (!recording){
            clone = _clone;
            return StartRecording();
        }
        else
            StopRecording();
            return true;
    }
    bool StartRecording(){
        if (allowedToRecord){
            recording = true;
            cumulDeltaTime = 0f;
            startPosition = transform.position;
            startRotation = transform.rotation;
            memoire = new List<PointInTime>();
            memoire.Add(new PointInTime(transform,ps.character.transform.rotation,m_animator.GetCurrentAnimatorStateInfo(0)));
            audioSource.PlayOneShot(startRecordSound, 0.7f);
            return true;
        }
        return false;
    }
    
    void StopRecording(){
        recording = false;
        audioSource.PlayOneShot(stopRecordSound, 0.7f);
    }

    public void allowRecording(ZoneClonage z){
        allowedToRecord = true;
    }
    public void forbidRecording(){
        allowedToRecord = true;
    }
    
    public void Revert(){
        transform.position = startPosition;
        transform.rotation = startRotation;
        GameObject instance  = Instantiate(clone);
        instance.transform.SetParent(GameObject.FindGameObjectWithTag("Level").transform);
        Physics.IgnoreCollision(GetComponent<Collider>(), instance.GetComponent<Collider>());
        Debug.Log("creating a clone with "+memoire.Count+" keyframes that last "+ sizeKeyframe+" frames");
        instance.GetComponent<CloneScript>().setMemoire(memoire,sizeKeyframe);  
        instance.GetComponent<CloneScript>().startPos = startPosition;
        instance.GetComponent<CloneScript>().startRot = startRotation;
        clones.Add(instance);
        //zone.addClone(instance);
    }

    public void removeAllClones(){
        foreach(GameObject g in clones){
            clones.Remove(g);
            Destroy(g);
            
        }

    }

    public void removeLastClone(){
        if (clones.Count==0) return;
        GameObject g = clones[clones.Count-1];
        clones.RemoveAt(clones.Count-1);
        Destroy(g);
        
    }
}
