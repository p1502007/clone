﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class monTMP : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       Transform target = GameObject.FindGameObjectsWithTag("MainCamera")[0].transform;

        Vector3 targetPostition = new Vector3(target.position.x,
                                        transform.position.y,
                                        target.position.z);

        transform.LookAt(targetPostition);
        transform.Rotate(0f, 180f, 0f);
    }
}
