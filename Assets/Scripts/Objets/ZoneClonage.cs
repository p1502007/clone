﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZoneClonage : Button
{
    
    public int nombreDeClonesMax = 1;
    public float recordTimeLimit = 10.0f;
    public GameObject clone;
    private int nbrDeClonesRestants;
    private GameObject[] listClones;
    private int nbrClonesCourant = 0;

    private void Start()
    {
        nbrDeClonesRestants = nombreDeClonesMax;
        GetComponentInChildren<TextMeshPro>().text = nbrDeClonesRestants.ToString();

        listClones = new GameObject[nombreDeClonesMax];
    }

    private void Update()
    {
        nbrDeClonesRestants = nombreDeClonesMax - nbrClonesCourant;
        GetComponentInChildren<TextMeshPro>().text = nbrDeClonesRestants.ToString();
    }

    public int getNbrClonesRestants()
    {
        return nbrDeClonesRestants;
    }

    public void setNbrClonesRestants(int nbr)
    {
        nbrDeClonesRestants = nbr;
    }

    public void addClone(GameObject newClone)
    {
        if(nbrClonesCourant < nombreDeClonesMax)
        {
            listClones[nbrClonesCourant++] = newClone;
        }

        else
        {
            Debug.Log("Nombre de clones max atteint");
        }
    }

    public void deleteLastClone()
    {
        if(nbrClonesCourant > 0)
        {
            listClones[nbrClonesCourant--] = null;
        }

        else
        {
            Debug.Log("Pas de clones à supprimer");
        }
    }

    public void clearAllClones()
    {
        if(nbrClonesCourant > 0)
        {
            for(int i = 0 ; i < nbrClonesCourant ; i++)
            {
                Destroy(listClones[i]);
            }
            nbrClonesCourant=0;
            nbrDeClonesRestants = nombreDeClonesMax;
        }

        else
        {
            Debug.Log("Il n'y a déjà plus aucun clone");
        }
    }

    public void playClones()
    {
        if(nombreDeClonesMax > 0)
        {
            for(int i = 0 ; i < nbrClonesCourant ; i++)
            {
                //listClones[i].GetComponent<CloneScript>().Go();
            }
        }
    }

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.gameObject.tag == "Player"){
            other.GetComponent<RecordScript>().allowRecording(this);
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
        if (other.gameObject.tag == "Player"){
            other.GetComponent<RecordScript>().forbidRecording();
        }
    }

    protected override void OnTriggerStay(Collider other)
    {
        base.OnTriggerStay(other);
        Debug.Log(other.gameObject.tag);
        if (other.gameObject.tag == "Player"){
            other.GetComponent<RecordScript>().allowRecording(this);
        }
    }
}
