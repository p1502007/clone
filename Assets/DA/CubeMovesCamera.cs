﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovesCamera : MonoBehaviour
{
    GameObject cube;

    protected Camera m_camera;
    protected Vector3 m_camera_target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    float x = 0.0f;
    float y = 0.0f;

    bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        m_camera = GetComponentInChildren<Camera>();
        cube = GameObject.Find("Cube");

        Vector3 angles = m_camera.transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cam_forward = m_camera.transform.forward;
        Vector3 cam_right = m_camera.transform.right;
        cam_forward.y = 0;
        cam_right.y = 0;
        transform.position += cam_forward * Input.GetAxis("Vertical") * 0.1f * (Input.GetAxis("Run") + 1f);
        transform.position += cam_right * Input.GetAxis("Horizontal") * 0.1f * (Input.GetAxis("Run") + 1f);

        if (grounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GetComponent<Rigidbody>().AddForce(0f, 500f, 0f);
            }
        }
        
        if (Input.GetAxis("Vertical") != 0f)
        {
            cube.transform.rotation = Quaternion.LookRotation(cam_forward);
        }
        /*if (Input.GetAxis("Horizontal") != 0f)
        {
            cube.transform.rotation = Quaternion.LookRotation(cam_right);
        }*/
    }

    private void LateUpdate()
    {
        m_camera_target = transform.position;

        x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

        y = ClampAngle(y, yMinLimit, yMaxLimit);

        Quaternion rotation = Quaternion.Euler(y, x, 0);

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

        /*RaycastHit hit;
        if (Physics.Linecast(m_camera_target, m_camera.transform.position, out hit))
        {
            distance -= hit.distance;
        }*/
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + m_camera_target;

        m_camera.transform.rotation = rotation;
        m_camera.transform.position = position;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    void OnCollisionEnter(Collision collision)
    {
        grounded = true;
    }

    void OnCollisionExit(Collision collision)
    {
        grounded = false;
    }

    void OnCollisionStay(Collision collision)
    {
        grounded = true;    
    }
}
