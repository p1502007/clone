﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionInTime
{
    public Vector3 direction;
    public float duration = 0f;
    

    public DirectionInTime(Vector3 v){
        direction = v;
    }
    public DirectionInTime(Vector3 v, float delta)
    {
        direction = v;
        duration = delta;
    }
}
