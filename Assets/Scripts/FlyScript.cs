﻿using UnityEngine;
using System.Collections;

public class FlyScript : MonoBehaviour
{
    public bool instantiate_clones = true;
    public float speed = 1f;
    public float rotateSpeed = 5.0f;
    public int nb_clones = 0;

    public Vector3 startPosition;
    Vector3 newPosition;

    void Start()
    {
        speed = Random.Range(0.1f, 0.5f);
        float scale = Random.Range(0.3f, 0.7f);
        transform.localScale = new Vector3(scale,scale,scale);
        startPosition = transform.position;
        PositionChange();

        if (instantiate_clones)
        {
            for (int i = 0; i < nb_clones; i++)
            {
                float diff = 100;

                float diff_x = Random.Range(-diff, diff);
                float diff_y = Random.Range(-diff, diff);
                float diff_z = Random.Range(-diff, diff);

                float pos_x = transform.position.x;
                float pos_y = transform.position.y;
                float pos_z = transform.position.z;

                Vector3 instantiate_position = new Vector3(pos_x + diff_x, pos_y + diff_y, pos_z + diff_z);
                GameObject copy = Instantiate(gameObject, instantiate_position, Quaternion.identity);
                FlyScript flyScript = copy.GetComponent<FlyScript>();
                flyScript.instantiate_clones = false;
                flyScript.startPosition = instantiate_position;
            }     
        }
        
    }

    void PositionChange()
    {
        newPosition = new Vector3(Random.Range(startPosition.x - 10, startPosition.x + 10), Random.Range(startPosition.y - 10, startPosition.y + 10), Random.Range(startPosition.z - 10, startPosition.z + 10));
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, newPosition) < 1)
            PositionChange();

        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);

        LookAt2D(newPosition);
    }

    void LookAt2D(Vector3 lookAtPosition)
    {
        float distanceX = lookAtPosition.x - transform.position.x;
        float distanceY = lookAtPosition.y - transform.position.y;
        float angle = Mathf.Atan2(distanceX, distanceY) * Mathf.Rad2Deg;

        Quaternion endRotation = Quaternion.AngleAxis(angle, Vector3.back);
        transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime * rotateSpeed);
    }
}