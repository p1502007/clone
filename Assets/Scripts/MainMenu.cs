﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject levelChanger;
    LevelChangerScript levelChangerScript;

    public void Start()
    {
        levelChangerScript = levelChanger.GetComponent<LevelChangerScript>();
    }

    public void PlayGame()
    {
        levelChangerScript.FadeToLevel(1);
    }

    public void Back()
    {
        levelChangerScript.FadeToLevel(0);
    }

    public void Tuto1()
    {
        levelChangerScript.FadeToLevel(2);
    }

    public void Level1()
    {
        levelChangerScript.FadeToLevel(3);
    }

    public void Level2()
    {
        levelChangerScript.FadeToLevel(4);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
